<?php

class Paginas extends Controller
{

    private $postsModel;

    public function __construct()
    {
        // Desde aquí cargaremos los modelos.
        $this->postsModel = $this->model('Post');
    }

    public function index(){
        $data = [
            "Titulo" => "Framework de Manuel Mañas Alfaro"
        ];

        if(isLoggedIn()){
            $data['posts'] = $this->postsModel->getPosts();
            $this->view('posts/index',$data);
        }else{
            $this->view('paginas/index',$data);
        }
        
    }

    public function about(){
        
        $this->view('paginas/about');
    }

}
