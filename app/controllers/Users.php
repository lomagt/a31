<?php

use \Tamtamchik\SimpleFlash\Flash;

// Permite usar una libreria para mensajes flash

class Users extends Controller
{

    private $userModel;

    public function __construct()
    {
        $this->userModel = $this->model('User');
    }

    public function register()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Combrueba si la variable global $_SERVER recibe una post y hará algo si es true

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'name' => trim($_POST['name']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',
            );

            if (empty($data['name'])) {
                $data['name_err'] = 'Please choose a username.';
            }

            if (empty($data['email'])) {
                $data['email_err'] = 'Please choose a email.';
            } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'Please choose a valid email.';
            } else if ($this->userModel->findUserByEmail($data['email'])) {
                $data['email_err'] = 'Please choose an unrepeated email.';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please provide a valid password.';
            } else if (isset($data['password'])) {
                if (strlen($data['password']) < 6) {
                    $data['password_err'] = 'Please provide a valid password with 6 or more digits.';
                }
            }

            if (empty($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Please repeat a valid password.';
            } else {
                if ($data['confirm_password'] != $data['password']) {
                    $data['confirm_password_err'] = 'Please repeat a valid password.';
                }
            }

            if (empty($data['name_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])) {
                
                //echo "to-do:registrar usuario";
                
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                $this->userModel->register($data);

                $flash = new Flash();
                $flash->message('Ya estás registrado y puedes iniciar sesión.', 'info');

                redirect('/users/login');
            } else {
                $this->view('users/register', $data);
            }

        } else { // Si no crea un array data asociativo con sus claves vacias
            $data = [
                'name' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => '',
            ];
            $this->view('users/register', $data); // Y ejecuta el método view de la clase padre pasandole el array como parámetro
        }
    }

    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Combrueba si la variable global $_SERVER recibe una post y hará algo si es true

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => '',
            );

            if (empty($data['email'])) {
                $data['email_err'] = 'Please choose a email.';
            } else if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'Please choose a valid email.';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please provide a valid password.';
            } else if (isset($data['password'])) {
                if (strlen($data['password']) < 6) {
                    $data['password_err'] = 'Please provide a valid password with 6 or more digits.';
                }
            }

            if (empty($data['email_err']) && empty($data['password_err'])) {

                if ($this->userModel->findUserByEmail($data['email'])) {

                    
                    $user = $this->userModel->login($data['email'], $data['password']);
                    if ($user) {
                        // Lo haremos más adelante: creamos variables de sesión
                        $this->createUserSession($user);
                    } else {
                        $data['password_err'] = 'Contraseña incorrecta';
                        $this->view('users/login', $data);
                    }

                } else {
                    $data['email_err'] = 'El usuario no se ha encontrado';
                }

            } else {
                $this->view('users/login', $data);
            }

        } else { // Si no crea un array data asociativo con sus claves vacias
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => '',
            ];
            $this->view('users/login', $data); // Y ejecuta el método view de la clase padre pasandole el array como parámetro
        }
    }

    public function createUserSession($user){
        
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_name'] = $user->name;
        $_SESSION['user_email'] = $user->email;

        redirect('/posts/index');
    }

    public function logout(){
        
        unset($_SESSION['user_id']);
        unset($_SESSION['user_name']);
        unset($_SESSION['user_email']);
        session_destroy();

        redirect('/paginas/index');
    }

}
