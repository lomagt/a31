<?php

use \Tamtamchik\SimpleFlash\Flash;

// Permite usar una libreria para mensajes flash

class Posts extends Controller
{
    private $postsModel;

    private $userModel;

    public function __construct()
    {

        if (!isLoggedIn()) {
            redirect('/users/login');
        }

        $this->postsModel = $this->model('Post'); // new model...

        $this->userModel = $this->model('User');
    }

    public function index()
    {
        $data = [
            "Titulo" => "Framework de Manuel Mañas Alfaro",
        ];

        $data['posts'] = $this->postsModel->getPosts();

        $this->view('posts/index', $data);
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Combrueba si la variable global $_SERVER recibe una post y hará algo si es true

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = array(
                'user_id' => trim($_SESSION['user_id']),
                'title' => trim($_POST['title']),
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '',
                'body' => trim($_POST['body']),
                'title_err' => '',
                'body_err' => '',
                'image_err' => '',
            );

            if (empty($data['title'])) {
                $data['title_err'] = 'Please choose a Title.';
            }

            if (empty($data['body'])) {
                $data['body_err'] = 'Please choose a Body.';
            }

            // if (!empty($data['image'])) {
            //     if ($_FILES['image']['error'] !== UPLOAD_ERR_OK) {
            //         switch ($_FILES['image']['error']) {
            //             case UPLOAD_ERR_INI_SIZE:
            //             case UPLOAD_ERR_FORM_SIZE:
            //                 $data['image_err'] = 'El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize');
            //                 break;
            //             case UPLOAD_ERR_PARTIAL:
            //                 $data['image_err'] = "No se ha podido subir el fichero completo";
            //                 break;
            //             default:
            //                 $data['image_err'] = "Error al subir el fichero";
            //                 break;
            //         }
            //     } else {
            //         $arrTypes = ["image/jpeg", "image/png", "image/gif"];
            //         if (in_array($_FILES['image']['type'], $arrTypes) === false) {
            //             $data['image_err'] = "Tipo de fichero no soportado";
            //         } else if (is_uploaded_file($_FILES['image']['tmp_name']) === false) {
            //             $data['image_err'] = "El archivo no se ha subido mediante un formulario";
            //         } else if (move_uploaded_file($_FILES['image']['tmp_name'], 'img/' . $_FILES['image']['name']) === false) {
            //             $data['image_err'] = "Ocurrió algún error al subir el fichero. No pudo guardarse.";
            //         }
            //     }
            // }

            if (!empty($data['image'])) {
                $arrTypes = ["image/jpeg", "image/png", "image/gif"];
                $newFile = new File($_FILES['image'], $arrTypes);
                try {

                    //Complétalo
                    $newFile->errorFile();
                    $newFile->saveUploadFile('img/');
                        
                } catch (FileException $error) {
                    $data['image_err'] = $error->getMessage();
                }
            }

            if (empty($data['title_err']) && empty($data['body_err']) && empty($data['image_err'])) {

                if ($this->postsModel->addPost($data)) {

                    $flash = new Flash();
                    $flash->message('Post insertado con exito.', 'info');

                    redirect('/posts/index');
                } else {

                    $flash = new Flash();
                    $flash->message('Error en la creación del post.', 'info');

                    redirect('/posts/index');
                }

            } else {
                $this->view('posts/add', $data);
            }

        } else { // Si no crea un array data asociativo con sus claves vacias
            $data = [
                'title' => '',
                'body' => '',
                'image' => '',
                'title_err' => '',
                'body_err' => '',
                'image_err' => '',
            ];
            $this->view('posts/add', $data); // Y ejecuta el método view de la clase padre pasandole el array como parámetro
        }

    }

    public function show($id)
    {
        $dataModelPost = $this->postsModel->getPostById($id);
        $dataModelUser = $this->userModel->getUserById($dataModelPost->user_id);

        $data = [
            'user' => $dataModelUser,
            'post' => $dataModelPost,
        ];
        $this->view('posts/show', $data);

    }

    public function edit($id)
    {

        $dataModelPost = $this->postsModel->getPostById($id);
        $dataModelUser = $this->userModel->getUserById($dataModelPost->user_id);

        $data = [
            'user_id' => $dataModelUser->id,
            'post_id' => $dataModelPost->id,
            'name_user' => $dataModelUser->name,
            'title' => $dataModelPost->title,
            'body' => $dataModelPost->body,
            'created_at' => $dataModelPost->created_at,
            'title_err' => '',
            'body_err' => '',
        ];

        if ($_SESSION['user_id'] === $dataModelPost->user_id) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $datos = array(
                    'user_id' => trim($_SESSION['user_id']),
                    'post_id' => $dataModelPost->id,
                    'title' => trim($_POST['title']),
                    'body' => trim($_POST['body']),
                    'title_err' => '',
                    'body_err' => '',
                );

                if (empty($datos['title'])) {
                    $datos['title_err'] = 'Please choose a Title.';
                }

                if (empty($datos['body'])) {
                    $datos['body_err'] = 'Please choose a Body.';
                }

                if (empty($datos['title_err']) && empty($datos['body_err'])) {

                    if ($this->postsModel->updatePost($datos)) {

                        $flash = new Flash();
                        $flash->message('Post actualizado con exito.', 'info');

                        redirect('/posts/index');
                    } else {

                        $flash = new Flash();
                        $flash->message('Error en la actualización del post.', 'info');

                        redirect('/posts/index');
                    }

                } else {
                    $this->view('posts/edit', $datos);
                }
            } else {
                $this->view('posts/edit', $data);
            }

        } else {

            redirect('/posts/index');

        }

    }

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if ($this->postsModel->deletePost($id)) {

                $flash = new Flash();
                $flash->message('Post Borrado con exito.', 'info');

                redirect('/posts/index');
            } else {

                $flash = new Flash();
                $flash->message('Error en el borrado del post.', 'info');

                redirect('/posts/index');
            }

        }
    }

}
