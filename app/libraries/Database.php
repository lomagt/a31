<?php

class Database
{

    private $host = DB_HOST;

    private $user = DB_USER;

    private $pass = DB_PASS;

    private $dbname = DB_NAME;

    // Database handler
    private $dbh;

    private $stmt;

    private $error;

    public function __construct()
    {
        // prepara la variable $dsn utilizando el prefijo mysql antes, para llamar al host que se esta utilizando y el nombre de la base de datos que se usa.
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        // prepara las opciones de PDO para hacer la llamada a la base de datos, configuración de el juego caracteres utf8
        // persitencia de la conexion true y atributo de modo de error para que devuelva execpciones de error en caso de que los haya.
        $options = [

            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", // Asigna el juego de caracteres utf8

            PDO::ATTR_PERSISTENT => true, // Establece que la conexion con la base de datos sea persistente lo que quiere decir que una vez se ternime de ejecutar
            // el script la conexión con la base de datos queda guardada en la cache para volver a ser reutilizada es caso
            // de que otro script solicitase conectarse con esa misma BBDD.

            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // Establece el atributo ATTR_ERRORMODE para que reporte errores lanzando excepciones

        ];

        // intenta la conexión mediate la instacia del objeto PDO con las variables privades anteriormente declaradas
        // devuelve un error si lo hay, gracias al atributo ATTR_ERRORMODE que llama al EXCEPTION, por la función getMessage();
        try {

            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);

        } catch (PDOException $e) {

            $this->error = $e->getMessage();

            echo $this->error;

        }

    }

    public function query(string $SQL)
    {

        $this->stmt = $this->dbh->prepare($SQL);

    }

    public function bind($param, $value, $type = null)
    { // El método se declara con tres parámetros, por defecto type siempre estará a null

        if (is_null($type)) { // solo entrará si type es null

            switch (true) { //por defecto el switch esta con true por lo que pasa a evaluar los case

                case is_int($value): // si $value es un entero entra.

                    $type = PDO::PARAM_INT; // el tipo de varialble que se le asigna a $type es PDO::PARAM_INT

                    break;

                case is_bool($value): // si $value es un boleano entra.

                    $type = PDO::PARAM_BOOL; // a $type se le asigna un PDO::PARAM_BOOL

                    break;

                case is_null($value): // si $value es un NULL entra

                    $type = PDO::PARAM_NULL; // se le asigna a $type un PDO::PARAM_NULL

                    break;

                default: // en caso de que no sea ninguna de las anteriores es un string

                    $type = PDO::PARAM_STR; // A $type se le asigna un PDO::PARAM_STR

            }

        }
        // bindValue prepara el valor a vincular con un atributo de una sentencia SQL para que se defina el tipo de dato
        // en caso de no ser vinculable porque el tipo no es el mismo bindValue devuelve false, en caso contrario devuelve true
        $this->stmt->bindValue($param, $value, $type);

    }

    public function execute()
    {

        return $this->stmt->execute();

    }

    // fetchAll devuelve un array que contiene todas las filas de el obejto al que apunta this
    // PDO::FETCH_CLASS es un atributo de estilo asociado a lo que devuelve fetchAll, hace que fetchAll devuelva una nueva instancia
    // de la clase solicitada. PDO::FETCH_PROPS_LATE fuerza a que el atributo PDO::FETCH_CLASS llame antes al constructor de la clase antes
    // de que las propiedades sean asignadas desde la columna respectiva.
    // En resumen fetchAll nos devuelve un array de objetos instanciados con la clase a la que hace referencia $this, que a su vez la propiedades
    // son asignadas a la clase despues de llamar al constructor de la clase referencida.
    public function resultSet($model)
    {

        $this->execute();

        return $this->stmt->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $model);

    }

    public function single($model)
    {

        $this->execute();

        $this->stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $model);

        return $this->stmt->fetch();

    }

    public function rowCount($stringModel)
    {
        $this->execute();

        $this->stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $stringModel);

        return $this->stmt->rowCount();
    }

}
