<?php

class File
{

    private $FileName;

    private $typeArray;

    public function __construct($fName, $tArray)
    {
        $this->FileName = $fName;
        $this->typeArray = $tArray;
    }

    public function errorFile()
    {
        if ($this->FileName['error'] !== UPLOAD_ERR_OK) {
            switch ($this->FileName['error']) {
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new FileException('El fichero es demasiado grande. Tamaño máximo: ' . ini_get('upload_max_filesize'));
                    break;
                case UPLOAD_ERR_PARTIAL:
                    throw new FileException("No se ha podido subir el fichero completo");
                    break;
                default:
                    throw new FileException("Error al subir el fichero");
                    break;
            }
        }

    }

    public function saveUploadFile($url)
    {

        if (in_array($this->FileName['type'], $this->typeArray) === false) {
            vardump('error');
            throw new FileException("Tipo de fichero no soportado");
        } else if (is_uploaded_file($this->FileName['tmp_name']) === false) {
            throw new FileException("El archivo no se ha subido mediante un formulario");
        } else if (move_uploaded_file($this->FileName['tmp_name'], $url . $this->FileName['name']) === false) {
            throw new FileException("Ocurrió algún error al subir el fichero. No pudo guardarse.");
        }

    }

}
