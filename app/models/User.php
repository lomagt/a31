<?php

class User extends Database
{

    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function findUserByEmail($email)
    {

        $this->db->query("SELECT email FROM users WHERE email = :email ");

        $this->db->bind(':email', $email);

        $this->db->execute();

        $this->db->single('User');

        if ($this->db->rowCount('User') > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register($data)
    {
        $this->db->query("INSERT INTO users(name, email, password) VALUES(:name, :email, :password)");

        $this->db->bind(':name', $data['name']);

        $this->db->bind(':email', $data['email']);

        $this->db->bind(':password', $data['password']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    public function login($email, $password)
    {
        $this->db->query("SELECT * FROM users WHERE email = :email ");

        $this->db->bind(':email', $email);

        $this->db->execute();

        $single = $this->db->single('User');

        if(password_verify($password, $single->password)){
            return $single;
        }else{
            return false;
        }
    }

    public function getUserById($id){
        
        $this->db->query('SELECT * FROM users WHERE id = :id');

        $this->db->bind(':id', $id);

        $user = $this->db->single('User');

        return $user;

    }

}
