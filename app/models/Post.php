<?php

class Post
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getPosts()
    {

        $this->db->query('
        SELECT *,
        posts.id as postId,
        posts.created_at as postCreatedAt,
        users.id as userId ,
        users.created_at as userCreatedAt
        FROM posts
        INNER JOIN users
        ON posts.user_id = users.id
        ORDER BY posts.created_at DESC
        ');

        $posts = $this->db->resultSet('Post');

        return $posts;

    }

    public function addPost($data)
    {
        $this->db->query('INSERT INTO posts (user_id, title, body, image) VALUES(:user_id, :title, :body, :image)');

        $this->db->bind(':user_id', $data['user_id']);

        $this->db->bind(':title', $data['title']);

        $this->db->bind(':body', $data['body']);
        
        $this->db->bind(':image', $data['image']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }

    }

    public function getPostById($id)
    {

        $this->db->query('SELECT * FROM posts WHERE id = :id');

        $this->db->bind(':id', $id);

        $post = $this->db->single('Post');

        return $post;

    }

    public function updatePost($data){
        $this->db->query('UPDATE posts SET title = :title, body = :body WHERE id = :post_id');

        $this->db->bind(':post_id', $data['post_id']);

        $this->db->bind(':title', $data['title']);

        $this->db->bind(':body', $data['body']);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }

    public function deletePost($id){

        $this->db->query('DELETE FROM posts WHERE id = :id');

        $this->db->bind(':id', $id);

        $bool = $this->db->execute();

        if ($bool) {
            return true;
        } else {
            return false;
        }
    }
    
}
