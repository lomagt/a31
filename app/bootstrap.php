<?php

// Start a Session
if (!session_id()) {
    @session_start();
}

// Initialize Composer Autoload
require_once 'vendor/autoload.php';

// Cargar el archivo .env 
$dotenv = Dotenv\Dotenv::createImmutable(dirname(dirname(__FILE__)));
$dotenv->load();

require_once 'helpers/utils.php';
require_once 'helpers/urlHelper.php';
require_once 'config/config.php';
require_once 'exceptions/FileException.php';

// spl_autoload_register(function ($className) {
//     
//  require_once 'libraries/' . $className . '.php';
//
// });

spl_autoload_register(function ($className) {

    $dirs = array(
        '../app/libraries/',
    );

    foreach ($dirs as $dir) {
        if (file_exists($dir . $className . '.php')) {
            require_once $dir . $className . '.php';
            return;
        }
    }

});

// require_once 'models/User.php';
// require_once 'models/Post.php';
