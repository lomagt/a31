<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">MVCPosts</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link <?= (esActiva('/paginas/index') || esActiva('/posts/index') ?'active':''); ?>" aria-current="page" href="<?= URLROOT ?>/paginas/index">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= (esActiva('/paginas/about')?'active':''); ?>" href="<?= URLROOT ?>/paginas/about">About</a>
        </li>
      </ul>
      <?php
        if(isLoggedIn()){
      ?>
      
      <div class="d-flex">        
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <span class="nav-link text-white">Username: <?= $_SESSION['user_name'] ?></span>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= URLROOT ?>/users/logout">Close Session</a>
          </li>
        </ul>
      </div>

      <?php
        }else{
      ?>

      <div class="d-flex">        
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="<?= URLROOT ?>/users/register">Registrar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= URLROOT ?>/users/login">Login</a>
          </li>
        </ul>
      </div>

      <?php
        }
      ?>
    </div>
  </div>
  
</nav>