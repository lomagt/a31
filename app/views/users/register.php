<!-- header page -->
<?php include_once APPROOT.'/views/partials/header.php'; ?>
<!-- Fin header page -->

<!-- CONTENT PAGE -->

<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2> Crear una cuenta</h2>
            <p>Por favor llena los campos para poder registrarse</p>
            <form action="<?= URLROOT ?>/users/register" method="post">
                <div class="form-group">
                    <label for="name">Nombre: <sup>*</sup></label>
                    <input type="text" name="name" class="form-control <?= !empty($datos['name_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['name']) ? $datos['name'] : '' ?>">  
                    <div class="invalid-feedback">
                        <?php echo $datos['name_err']  ?>
                    </div>                  
                </div>
                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="text" name="email" class="form-control <?= !empty($datos['email_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['email']) ? $datos['email'] : '' ?>">
                    <div class="invalid-feedback">
                       <?= $datos['email_err'] ?>
                    </div>                    
                </div>
                <div class="form-group">
                    <label for="password">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?= !empty($datos['password_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['password']) ? $datos['password'] : '' ?>">
                    <div class="invalid-feedback">
                       <?= $datos['password_err'] ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirm_password">Confirmar contraseña: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control <?= !empty($datos['confirm_password_err']) ? 'is-invalid' : '' ?>" value="<?= !empty($datos['confirm_password']) ? $datos['confirm_password'] : '' ?>"> 
                    <div class="invalid-feedback">
                       <?= $datos['confirm_password_err'] ?>
                    </div>                   
                </div>
                <div class="row mt-4">
                    <div class="col">
                        <a href="<?= URLROOT ?>/users/login">¿Ya tienes cuenta? Inicia sesión</a>
                    </div>
                    <div class="col">
                        <input type="submit" value="Registrar" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- FIN CONTENT PAGE -->

<!-- Fooder page -->
<?php include_once APPROOT.'/views/partials/footer.php'; ?>
<!-- Fooder page -->