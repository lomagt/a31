<!-- header page -->
<?php include_once APPROOT.'/views/partials/header.php'; ?>
<!-- Fin header page -->

<!-- CONTENT PAGE -->

    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5">
               
                <h2>Iniciar sesión</h2>
                <p>Por favor, introduzca su correo y contraseña</p>
                <form action="<?= URLROOT ?>/users/login" method="post">
                    <div class="form-group">
                        <label for="email">Email: <sup>*</sup></label>
                        <input type="text" name="email" class="form-control <?= !empty($datos['email_err']) ? 'is-invalid' : '' ?>" placeholder="Su correo electrónico" value="<?= !empty($datos['email']) ? $datos['email'] : '' ?>">
                        <div class="invalid-feedback">
                        <?= $datos['email_err'] ?>
                        </div>                        
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña: <sup>*</sup></label>
                        <input type="password" name="password" class="form-control <?= !empty($datos['password_err']) ? 'is-invalid' : '' ?>" placeholder="Su contraseña" value="<?= !empty($datos['password']) ? $datos['password'] : '' ?>">
                        <div class="invalid-feedback">
                        <?= $datos['password_err'] ?>
                        </div>                        
                    </div>
                    <div class="row mt-4">
                        <div class="col">
                            <a href="<?= URLROOT ?>/users/register">¿No tienes cuenta? Regístrate</a>
                        </div>
                        <div class="col">
                            <input type="submit" value="Iniciar sesión" class="btn btn-primary btn-block">
                        </div>
                    </div>
                </form>
                
                <div class="flashes">
                    <?= (string) flash() ?>
                </div>

            </div>
        </div>
    </div>

<!-- FIN CONTENT PAGE -->

<!-- Fooder page -->
<?php include_once APPROOT.'/views/partials/footer.php'; ?>
<!-- Fooder page -->