<!-- header page -->
<?php include_once APPROOT . '/views/partials/header.php';?>
<!-- Fin header page -->

<div class="container">
<a class="btn btn-warning pull-right mt-3" href="<?=URLROOT?>/posts/index" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<br>

<div class="row mb-3">
    <div class="col-md-12">
        <h1></h1>

        <div class="bg-secondary text-white p-2 mb-3">
            Creado por: <?=!empty($datos['user']->name) ? $datos['user']->name : ''?> el <?=!empty($datos['post']->created_at) ? $datos['post']->created_at : ''?>
        </div>
        <h3>
            <?=!empty($datos['post']->title) ? $datos['post']->title : ''?>
        </h3>
        <p>
            <?php
                if(!empty($datos['post']->image)){
            ?>
            <img src="<?=URLROOT?>/public/img/<?= $datos['post']->image ?>" alt="<?= $datos['post']->image ?>" class="float-start" width="200px">
            <?php
                }
            ?>
            <?=!empty($datos['post']->body) ? $datos['post']->body : ''?>
        </p>

            <hr>
            <?php
if ($_SESSION['user_id'] === $datos['user']->id) {
    ?>
            <div class="row">
                <div class="col">
                    <a href="<?=URLROOT?>/posts/edit/<?=$datos['post']->id?>" class="btn btn-success btn-block">
                        <i class="fas fa-edit"></i> editar
                    </a>
                </div>
                <div class="col">
                    <form action="<?=URLROOT?>/posts/delete/<?=$datos['post']->id?>" method="post">
                        <button type="submit" class="btn btn-danger btn-block">
                            <i class="fas fa-trash"></i> Borrar post
                        </button>
                    </form>
                </div>
            </div>
            <?php
}
?>
    </div>
</div>
</div>
<!-- Fooder page -->
<?php include_once APPROOT . '/views/partials/footer.php';?>
<!-- Fooder page -->