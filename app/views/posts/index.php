<!-- header page -->
<?php include_once APPROOT.'/views/partials/header.php'; ?>
<!-- Fin header page -->

<!-- NAVBAR -->
<?php include_once APPROOT.'/views/partials/navbar.php'; ?>
<!-- FIN NAVBAR -->
<div class="container">

<div class="row mb-3 mt-3">
    <div class="col-md-6">
        <h1>Publicaciones</h1>
    </div>
    <div class="col-md-6">
        <a class="btn btn-primary pull-right" href="<?= URLROOT ?>/posts/add" role="button">
            <i class="fas fa-pencil-alt"></i> Crear publicación
        </a>
    </div>
</div>
<div class="flashes mt-2 mb-2">
  <?= (string) flash() ?>
</div>
<div id="post" class="row mt-3">
<?php
    foreach($datos['posts'] as $obj){
?>
<div class="col-6 mt-3">
<div class="card text-center">
  <div class="card-header">
    Creado por: <?= $obj->name ?>
  </div>
  <div class="card-body">
    <h5 class="card-title"><?= $obj->title ?></h5>
    <p class="card-text">
      <?php
        if(!empty($obj->image)){
      ?>
      <img src="<?= URLROOT ?>/public/img/<?= $obj->image ?>" alt="<?= $obj->image ?>" class="float-start" width="250">
      <?php
        }
      ?>
      <?= $obj->body ?>
    </p>
    <a href="<?= URLROOT ?>/posts/show/<?= $obj->postId ?>" class="btn btn-primary">Ver Más</a>    
  </div>
  <div class="card-footer text-muted">
    <?= $obj->postCreatedAt ?>
  </div>
</div>
</div>
<?php
    }
?>
</div>
</div>
<!-- Fooder page -->
<?php include_once APPROOT.'/views/partials/footer.php'; ?>
<!-- Fooder page -->