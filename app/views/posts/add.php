<!-- header page -->
<?php include_once APPROOT.'/views/partials/header.php'; ?>
<!-- Fin header page -->

<!-- NAVBAR -->
<?php include_once APPROOT.'/views/partials/navbar.php'; ?>
<!-- FIN NAVBAR -->

<a class="btn btn-warning pull-right" href="<?= URLROOT ?>/posts/index" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5">
    <h2>Crear publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
    <form action="<?= URLROOT ?>/posts/add" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title">Título: <sup>*</sup></label>
            <input type="text" name="title" class="form-control <?= !empty($datos['title_err']) ? 'is-invalid' : '' ?>" placeholder="Título de la publicación" value="<?= !empty($datos['title']) ? $datos['title'] : '' ?>">
            <span class="invalid-feedback">
            <?= $datos['title_err'] ?>
            </span>
        </div>
        <div class="form-group">
            <label for="body">Contenido: <sup>*</sup></label>
            <textarea name="body" class="form-control <?= !empty($datos['body_err']) ? 'is-invalid' : '' ?>" rows="5" placeholder="Su contenido" value="">
            <?= !empty($datos['body']) ? $datos['body'] : '' ?>
            </textarea>

            <span class="invalid-feedback">
            <?= $datos['body_err'] ?>
            </span>
        </div>
        <div class="form-group">
            <label for="image">Imagen: <sup>*</sup></label>
            <input type="file" name="image" class="form-control <?= !empty($datos['image_err']) ? 'is-invalid' : '' ?>" placeholder="Título de la publicación" value="<?= !empty($datos['image']) ? $datos['image'] : '' ?>">
            <!-- <input type="hidden" name="MAX_FILE_SIZE" value="30000"> -->
            <span class="invalid-feedback">
            <?= $datos['image_err'] ?>
            </span>
        </div>
        <div class="row mt-3">
            <div class="col">
                <input type="submit" value="Crear publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>

<!-- Fooder page -->
<?php include_once APPROOT.'/views/partials/footer.php'; ?>
<!-- Fooder page -->