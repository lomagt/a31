<!-- header page -->
<?php include_once APPROOT.'/views/partials/header.php'; ?>
<!-- Fin header page -->

<!-- NAVBAR -->
<?php include_once APPROOT.'/views/partials/navbar.php'; ?>
<!-- FIN NAVBAR -->

<!-- CONTENT PAGE -->

    <div class="jumbotron jumbotron-fluid p-5">
        <div class="container-fluid bg-light p-5 mb-4 rounded-3">
            <h1 class="display-4">Página sobre nosotros</h1>
            <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <hr class="my-4">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
            <p class="lead">
                <span>Version: </span><a class="btn btn-success p-0" href="#" role="button"><?= APPVERSION ?></a>
            </p>
        </div>
    </div>

<!-- FIN CONTENT PAGE -->
<!-- Fooder page -->
<?php include_once APPROOT.'/views/partials/footer.php'; ?>
<!-- Fooder page -->